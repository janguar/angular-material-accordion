import { Component, OnInit } from '@angular/core';
import { ICountry } from '../county.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  selectedIndex: number | undefined;   // Step 1
  countries: ICountry[] = [];

  constructor() { }

  ngOnInit(): void {
    fetch('./assets/countries.json').then(res => res.json())
      .then(json => {
        this.countries = json;
      });
  }

  expansionPanelIndex(index: number) { // Step 2
    this.selectedIndex = index;
  }

}
